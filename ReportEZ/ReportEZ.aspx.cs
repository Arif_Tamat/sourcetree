﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using Microsoft.Reporting.WebForms;

namespace ReportEZ
{
    public partial class ReportEZ : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          if (!IsPostBack)
            {
                getWarehouse();
                ReportViewer1.Visible = false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
           
            GetData();
            ReportViewer1.Visible = true;
        }

        private void GetData()
        {
            String ConnString = System.Configuration.ConfigurationManager.AppSettings["ConnString"].ToString();
            string sql = "";

            //กำหนด Parameter ใส่ Report
                Microsoft.Reporting.WebForms.ReportParameter[] rPara = new Microsoft.Reporting.WebForms.ReportParameter[]
            {
                new Microsoft.Reporting.WebForms.ReportParameter("Header", "Customer"),
                new Microsoft.Reporting.WebForms.ReportParameter("Warehouse",ddlWarehouse.SelectedValue.ToString()),
                new Microsoft.Reporting.WebForms.ReportParameter("hey", "Welcome"),

            };

            OleDbConnection con = new OleDbConnection(ConnString);
            sql = "select Id, FirstName, LastName, Age, Telephone from Persons where 1=1";

            if (txtUserID.Text.Trim() != "")
            {
                sql += "AND Id='" + txtUserID.Text.Trim() + "'";
            }

            if (ddlWarehouse.SelectedIndex > 0)
            {
                txtUserID.Text = "";
                sql += "AND FirstName='" + ddlWarehouse.SelectedValue.ToString() + "'";
               
            }

            OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("RDLC/ReportPersons.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DSPersons", dt));

            //ใส่ Parameter ลง Report
            ReportViewer1.LocalReport.SetParameters(rPara);
            ReportViewer1.LocalReport.Refresh();
        }


        private void getWarehouse()
        {
             Class_RESOURCE clsRS = new Class_RESOURCE();

            String SQL = "select distinct FirstName as FirstName from Persons ";
            //SQL += "where status = 'Y'";
           // SQL += " Order by WAREHOUSEID ";
            OleDbConnection Conn = clsRS.ConnectionOpen();
            DataSet dsCustType = clsRS.BindData3(ref Conn, SQL);
            clsRS.ConnectionClose(ref Conn);
            Conn = null;
            clsRS = null;

            DataSet dsCustType2 = dsCustType.Copy();

            ddlWarehouse.DataSource = dsCustType;
            //ddlWarehouse.DataTextField = "FirstName";
            ddlWarehouse.DataValueField = "FirstName";
            ddlWarehouse.DataBind();
            ddlWarehouse.Items.Insert(0, "All");
            ddlWarehouse.Items[0].Value = "0";
    
        }
    }



}