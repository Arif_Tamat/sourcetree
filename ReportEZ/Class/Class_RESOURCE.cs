﻿/* class_resource v.1*/
/* by chotichai kraisuraphong*/

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace ReportEZ
{
    public class Class_RESOURCE
    {
        public OleDbCommand Comm;
        public int CommandTimeout = 300;
        public String ConnString = ConfigurationSettings.AppSettings["ConnString"].ToString();
        //SECOND  60=1MIN  240=4MINS   300=5 MINS

        public String CurrentSQLStatement;
        public OleDbTransaction BeginTran(ref OleDbConnection Conn)
        {
            OleDbTransaction tr1 = Conn.BeginTransaction(IsolationLevel.ReadUncommitted);
            return tr1;
        }

        //CHOTICHAI แก้ไข default table name = tablespacename
        public Boolean BindData(ref OleDbConnection Conn, ref DataSet DS, String SQL, String TABLESPACENAME)
        {
            Boolean RET = true;
            String TableName = TABLESPACENAME;

            if (TableName.Equals(""))
            {
                TableName = "BTABLE1";
            }

            try
            {
                OleDbDataAdapter DA1 = new OleDbDataAdapter(SQL, Conn);
                DA1.SelectCommand.CommandTimeout = CommandTimeout;
                DA1.Fill(DS, TableName);
            }
            catch (Exception ex)
            {
                RET = false;
            }

            return RET;
        }

        public DataSet BindData2(ref OleDbConnection Conn, String CustomSQLStatement, String TableName)
        {
            DataSet DS1 = new DataSet();
            String SqlStr = "";

            if (CustomSQLStatement == "")
            {
                SqlStr = "SELECT * FROM ";
            }
            else
            {
                SqlStr = CustomSQLStatement;
            }
            CurrentSQLStatement = SqlStr;
            OleDbDataAdapter DA1 = new OleDbDataAdapter(SqlStr, Conn);
            DA1.Fill(DS1, TableName);
            if (DS1.Tables[TableName].Rows.Count <= 0)
            {
                DA1 = null;
                DS1 = null;
            }
            return DS1;
        }

        public DataSet BindData2(ref OleDbConnection Conn, String CustomSQLStatement, ref String MessageError)
        {
            OleDbDataAdapter DA1;
            DataSet DS1 = new DataSet();
            String SqlStr = "";
            String TableName = "BTABLE1";

            try
            {
                if (CustomSQLStatement == "")
                {
                    SqlStr = "SELECT * FROM ";
                }
                else
                {
                    SqlStr = CustomSQLStatement;
                }
                CurrentSQLStatement = SqlStr;
                DA1 = new OleDbDataAdapter(SqlStr, Conn);
                DA1.SelectCommand.CommandTimeout = CommandTimeout;
                DA1.Fill(DS1, TableName);
                if (DS1.Tables[TableName].Rows.Count <= 0)
                {
                    DA1 = null;
                    DS1 = null;
                }
            }
            catch (Exception ex)
            {
                MessageError = ex.Message;
                DA1 = null;
                DS1 = null;
            }

            return DS1;
        }

        public DataSet BindData2(ref OleDbConnection Conn, String CustomSQLStatement)
        {
            DataSet DS1 = new DataSet();
            String SqlStr = "";
            String TableName = "BTABLE1";

            if (CustomSQLStatement == "")
            {
                SqlStr = "SELECT * FROM ";
            }
            else
            {
                SqlStr = CustomSQLStatement;
            }

            CurrentSQLStatement = SqlStr;
            OleDbDataAdapter DA1 = new OleDbDataAdapter(SqlStr, Conn);
            DA1.SelectCommand.CommandTimeout = CommandTimeout;
            DA1.Fill(DS1, TableName);

            if (DS1.Tables[TableName].Rows.Count <= 0)
            {
                DA1 = null;
                DS1 = null;
            }
            return DS1;
        }

        public DataSet BindDataTran(ref OleDbConnection Conn, String CustomSQLStatement, ref OleDbTransaction Tran)
        {
            DataSet DS1 = new DataSet();
            String SqlStr = "";
            String TableName = "BTABLE1";

            if (CustomSQLStatement == "")
            {
                SqlStr = "SELECT * FROM ";
            }
            else
            {
                SqlStr = CustomSQLStatement;
            }

            CurrentSQLStatement = SqlStr;

            OleDbDataAdapter DA1 = new OleDbDataAdapter(SqlStr, Conn);
            DA1.SelectCommand.Transaction = Tran;
            DA1.SelectCommand.CommandTimeout = CommandTimeout;
            DA1.Fill(DS1, TableName);
            if (DS1.Tables[TableName].Rows.Count <= 0)
            {
                DA1 = null;
                DS1 = null;
            }
            return DS1;
        }

        public DataSet BindData2(ref OleDbConnection Conn, String CustomSQLStatement, int timeout)
        {
            DataSet DS1 = new DataSet();
            String SqlStr = "";
            String TableName = "BTABLE1";

            if (CustomSQLStatement == "")
            {
                SqlStr = "SELECT * FROM ";
            }
            else
            {
                SqlStr = CustomSQLStatement;
            }
            CurrentSQLStatement = SqlStr;

            OleDbDataAdapter DA1 = new OleDbDataAdapter(SqlStr, Conn);
            DA1.SelectCommand.CommandTimeout = timeout;
            DA1.Fill(DS1, TableName);

            if (DS1.Tables[TableName].Rows.Count <= 0)
            {
                DA1 = null;
                DS1 = null;
            }

            return DS1;
        }

        public DataSet BindData3(ref OleDbConnection Conn, String CustomSQLStatement)
        {
            OleDbDataAdapter DA1;
            DataSet DS1 = new DataSet();
            String SqlStr = "";
            String TableName = "BTABLE1";

            if (CustomSQLStatement == "")
            {
                SqlStr = "SELECT * FROM ";
            }
            else
            {
                SqlStr = CustomSQLStatement;
            }
            CurrentSQLStatement = SqlStr;
            DA1 = new OleDbDataAdapter(SqlStr, Conn);
            DA1.SelectCommand.CommandTimeout = CommandTimeout;
            DA1.Fill(DS1, TableName);
            return DS1;
        }

        public void BindData3(ref OleDbConnection Conn, String CustomSQLStatement, ref DataSet dsresult)
        {
            dsresult = new DataSet();
            String SqlStr = "";
            String TableName = "BTABLE1";

            if (CustomSQLStatement == "")
            {
                SqlStr = "SELECT * FROM ";
            }
            else
            {
                SqlStr = CustomSQLStatement;
            }
            CurrentSQLStatement = SqlStr;
            OleDbDataAdapter DA1 = new OleDbDataAdapter(SqlStr, Conn);
            DA1.SelectCommand.CommandTimeout = CommandTimeout;
            DA1.Fill(dsresult, TableName);
        }

        //chotichai 2017-12-01 add parameter support
        public DataSet BindData4(ref OleDbConnection Conn, String CustomSQLStatement, ref OleDbParameterCollection Param1, ref String MessageError)
        {
            OleDbDataAdapter DA1;
            DataSet DS1 = new DataSet();
            String SqlStr = "";
            String TableName = "BTABLE1";

            try
            {
                if (CustomSQLStatement == "")
                {
                    SqlStr = "SELECT * FROM ";
                }
                else
                {
                    SqlStr = CustomSQLStatement;
                }

                CurrentSQLStatement = SqlStr;
                DA1 = new OleDbDataAdapter(SqlStr, Conn);
                DA1.SelectCommand.CommandTimeout = CommandTimeout;

                if (Param1 != null)
                {
                    if (Param1.Count > 0)
                    {
                        for (int ii = 0; ii < Param1.Count; ii++)
                        {
                            DA1.SelectCommand.Parameters.Add(Param1[ii]);
                        }
                    }
                }

                DA1.Fill(DS1, TableName);
                if (DS1.Tables[TableName].Rows.Count <= 0)
                {
                    DA1 = null;
                    DS1 = null;
                }
            }
            catch (Exception ex)
            {
                MessageError = ex.Message;
                DA1 = null;
                DS1 = null;
            }

            return DS1;
        }

        public Boolean CommitTran(ref OleDbTransaction Transaction1)
        {
            Boolean ret = false;
            try
            {
                Transaction1.Commit();
                ret = true;
            }

            catch (Exception ex)
            {
                Transaction1.Rollback();
                ret = false;
            }
            return ret;
        }

        public void ConnectionClose(ref OleDbConnection Conn)
        {
            if (Conn != null)
            {
                Conn.Close();
                Conn.Dispose();
            }
        }

        public OleDbConnection ConnectionOpen()
        {
            OleDbConnection Conn = new OleDbConnection();
            try
            {
                Conn = new OleDbConnection(ConnString);
                Conn.Open();
            }
            catch (Exception ex)
            {
                Conn = null;
            }
            return Conn;
        }

        public OleDbConnection ConnectionOpen(String val)
        {
            OleDbConnection Conn;
            try
            {
                Conn = new OleDbConnection(val);
                Conn.Open();
            }
            catch (Exception ex)
            {
                Conn = null;
            }
            return Conn;
        }

        public decimal ConvertNullToDecimal(object Charvalue)
        {
            decimal number = 0;
            try
            {
                Charvalue = Charvalue.ToString().Replace(",", "");

                if (Charvalue + "" == "")
                {
                    number = 0;
                }
                else
                {
                    number = decimal.Parse(Charvalue + "");
                }
            }
            catch (Exception)
            {
            }

            return number;
        }

        public Boolean ExecSql(ref OleDbConnection Conn, String SqlStatement, ref OleDbTransaction Transaction1, ref String MessageError)
        {
            Boolean Ret = true;
            //แก้ไขไปเรียกใช้ execsql4

            OleDbParameterCollection para = null;

            if (ExecSql4(ref Conn, SqlStatement, ref Transaction1, ref para, ref MessageError) == -1)
            {
                Ret = false;
            }

            //ถ้ามี ERROR
            if (!MessageError.Equals(""))
            {
                Ret = false;
            }

            return Ret;
        }

        public Boolean ExecSql(ref OleDbConnection Conn, String SqlStatement, OleDbTransaction Transaction1)
        {
            Boolean Ret = true;
            //แก้ไขไปเรียกใช้ execsql4
            String MessageError = "";

            OleDbParameterCollection para = null;

            if (ExecSql4(ref Conn, SqlStatement, ref Transaction1, ref para, ref MessageError) == -1)
            {
                Ret = false;
            }

            //ถ้ามี ERROR
            if (!MessageError.Equals(""))
            {
                Ret = false;
            }

            return Ret;
        }

        public Int32 ExecSql3(ref OleDbConnection Conn, String SqlStatement, ref OleDbTransaction Transaction1, ref String MessageError)
        {
            //แก้ไขไปเรียกใช้ execsql4
            OleDbParameterCollection para = null;

            return ExecSql4(ref Conn, SqlStatement, ref Transaction1, ref para, ref MessageError);
        }

        public Int32 ExecSql3(ref OleDbConnection Conn, String SqlStatement, ref String MessageError)
        {
            //แก้ไขไปเรียกใช้ execsql4
            OleDbTransaction tran = null;
            OleDbParameterCollection para = null;

            return ExecSql4(ref Conn, SqlStatement, ref tran, ref para, ref MessageError);
        }

        public Int32 ExecSql4(ref OleDbConnection Conn, String SqlStatement, ref OleDbTransaction Transaction1, ref OleDbParameterCollection Param1, ref String MessageError)
        {
            Int32 Ret = -1;
            MessageError = ""; //RESET TO NO ERROR
            try
            {
                Comm = new OleDbCommand(SqlStatement, Conn);
                if (Transaction1 != null)
                {
                    Comm.Connection = Conn;
                    Comm.Transaction = Transaction1;
                    Comm.CommandTimeout = CommandTimeout;
                }

                if (Param1 != null)
                {
                    if (Param1.Count > 0)
                    {
                        for (int ii = 0; ii < Param1.Count; ii++)
                        {
                            Comm.Parameters.Add(Param1[ii]);
                        }
                    }
                }

                Ret = Comm.ExecuteNonQuery();
                Comm.Dispose();
                Comm = null;
            }
            catch (OleDbException e)
            {
                MessageError = e.Message;
                Ret = -1;
            }
            catch (Exception ex)
            {
                MessageError = ex.Message;
                Ret = -1;
            }
            return Ret;
        }

        public String GETDBTYPE()
        {
            String dbtype = "SQLSERVER";

            try
            {
                dbtype = ConfigurationSettings.AppSettings["dbtype"].ToString();
            }
            catch (Exception ex)
            {
                dbtype = "SQLSERVER";
            }

            return dbtype;
        }

        public Boolean RollBackTran(ref OleDbTransaction Transaction1)
        {
            Boolean functionReturnValue = false;
            Boolean ret = false;
            try
            {
                Transaction1.Rollback();
                ret = true;
            }
            catch (Exception e)
            {
                ret = false;
                throw e;
            }
            finally
            {
                functionReturnValue = ret;
            }
            return functionReturnValue;
        }

        public string StrDDMMYYYYHHMMSStoYYYYMMDD(String d3)
        {
            String[] daypart = d3.Split('/');
            if (Convert.ToInt32(daypart[2]) > 2500)
            {
                d3 = Convert.ToString(Convert.ToInt32(daypart[2]) - 543) + daypart[1] + daypart[0];
            }
            else
            {
                d3 = Convert.ToString(Convert.ToInt32(daypart[2])) + daypart[1] + daypart[0];
            }
            return d3;
        }
        /*2016-07-27*/
        /* CHOTICHAI เพิ่ม oldbcollection เพื่อเก็บ parameter ที่ส่งมา*/
        internal bool BindData(ref OleDbConnection Conn, ref DataSet DS1, String TableName, String ConditionString, String CustomSQLStatement, String TableSpaceName, ref String MsgErr)
        {
            MsgErr = "";
            OleDbDataAdapter DA1;
            Boolean RET = true;
            String SqlStr = CustomSQLStatement + TableSpaceName + " " + ConditionString;
            try
            {
                CurrentSQLStatement = SqlStr;
                DA1 = new OleDbDataAdapter(SqlStr, Conn);
                DA1.SelectCommand.CommandTimeout = CommandTimeout;
                DA1.Fill(DS1, TableName);
                if (DS1.Tables[TableName].Rows.Count > 0)
                {
                    DA1 = null;
                }
                else
                {
                    if (TableName == "BCBarcodeMaster" || TableName == "tbDataReportRate" || TableName == "PHOTOCUST" || TableName == "AVGSTOCK" || TableName == "AVGPROCUST")
                    {
                        DA1 = null;
                    }
                    else
                    {
                        DA1 = null;
                        RET = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MsgErr = ex.ToString();
                DA1 = null;
                DS1 = null;
                RET = false;
            }
            return RET;
        }
    }
}