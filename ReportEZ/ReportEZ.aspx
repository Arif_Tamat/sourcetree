﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportEZ.aspx.cs" Inherits="ReportEZ.ReportEZ" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:Label ID="UserID" runat="server" Text="ID"></asp:Label>&nbsp;
            <asp:TextBox ID="txtUserID" type="number" runat="server"></asp:TextBox>&nbsp;

             <asp:Label ID="Label1" runat="server" Text="Warehouse"></asp:Label>&nbsp;
            <asp:DropDownList ID="ddlWarehouse" runat="server">
            </asp:DropDownList>

            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
            <br />
            
            <br />
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
</div>
    </form>
</body>
</html>
